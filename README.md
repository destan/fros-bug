# FROS需求和BUG管理

#### 介绍    
该项目用于维护FROS系统需求和BUG，方便统一管理，能够提高开发效率。  

#### 如何提交    
- 注册一个gitee网站账号  
- 点击该项目上方的Issues菜单  
- 点击新建Issue  
- 按照Issue模板填写问题描述提交即可  

#### 何时修复    
对于重大问题或者热度较高的需求会优先处理，注意如果发现已经有了相同或者类似的Issue，直接在Issue下评论即可。  
由于时间有限，有些问题处理的周期会较长，望理解，所以尽量提交合理的、受众范围广的需求。  

#### 不予处理的需求  
1. kexue插件集成，如果自行安装出现依赖问题，可以将依赖的系统插件名发出来。  
2. 大型插件集成，比如adguard等，占用内存多，影响稳定性的插件。 









